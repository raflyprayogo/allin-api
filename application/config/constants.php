<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');
defined('GCM_URL')      	   OR define('GCM_URL', 'https://fcm.googleapis.com/fcm/send'); // highest automatically-assigned error code
defined('GCM_API')      	   OR define('GCM_API', 'AAAAh-Lbtpo:APA91bGCEJDa9v90t1fw5gmC4Dx7QB3ZRfJwwXp0xZN63TGDSOzFYqhaoITfLo5UJxH7JEH7lxZzBh4tILVTuAetY9kNIZVnfXJAIB11gXFzU_hUMOyKIvWvITcw4GqU39GJda8Wqyrs'); // highest automatically-assigned error code

// ASSETS PATH
defined('BASE_URL_ASSETS')      	   	OR define('BASE_URL_ASSETS', './assets/');
defined('URL_TOKENS')      	   			OR define('URL_TOKENS', BASE_URL_ASSETS.'tokens/');
/* End of file constants.php */
/* Location: ./application/config/constants.php */