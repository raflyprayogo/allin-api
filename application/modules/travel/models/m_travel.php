<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_travel extends CI_Model {

    public function __construct(){
        parent::__construct();
        $this->headers          = $this->input->request_headers();
    }

    public function get_recomendation($lat, $long){
		$sql = "SELECT service_id id, service_name name, seller_name seller, seller_latitude latitude, seller_longitude longitude 
				, (3956 * 2 * ASIN(SQRT( POWER(SIN(( $lat - seller_latitude) *  pi()/180 / 2), 2) +COS( $lat * pi()/180) * COS(seller_latitude * pi()/180) * POWER(SIN(( $long - seller_longitude) * pi()/180 / 2), 2) ))) as distance  
				FROM services
				INNER JOIN sellers ON service_seller_id=seller_id
				WHERE service_status = '1'
				HAVING  distance <= 1000000 
				ORDER BY distance 
				LIMIT 10";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
}