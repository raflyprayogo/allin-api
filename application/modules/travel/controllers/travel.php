<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Travel extends CI_Controller {

	function __construct() {
        parent::__construct();
        $this->headers          = $this->input->request_headers();
        $this->api_version      = $this->headers['Api-Version'];
        $this->load->model('m_travel');
        date_default_timezone_set('UTC');
        header('Content-Type: application/json');
        acc_token();
    }
    

    public function index(){   
        echo "Dolan Travel Rest API";
    }

    function category(){
        if($this->api_version == '1'){
            $select         = 'travelcat_id id, travelcat_name name, travelcat_icon_url icon';
            $res_data       = $this->m_global->get_data_all('travel_categories', null, ['travelcat_status' => '1'], $select);
            echo response_builder(true, 200, $res_data);
        }else{
            echo response_builder(false, 900);
        }
    }

    function active_services(){
        if($this->api_version == '1'){
            $query          = $this->input->post('search');
            $seller         = $this->input->post('seller_id');
            $storefront     = $this->input->post('storefront');
            $cat_id         = $this->input->post('category_id');

            $select         = 'service_id id, service_name name, seller_name seller, seller_latitude latitude, seller_longitude longitude';
            $join           = [
                                    ['table' => 'sellers', 'on' => 'service_seller_id=seller_id']
                                ];

            if($cat_id != ''){
                $where['service_category_id'] = $cat_id;
            }

            if($query != ''){
                $where['service_name LIKE '] = '%'.$query.'%';
            }

            if($seller != ''){
                $where['seller_id'] = $seller;
            }

            if($storefront != ''){
                $where['service_storefront_id'] = $storefront;
            }

            $where['service_status']  = '1';

            $res_data       = $this->m_global->get_data_all('services', $join, $where, $select);
            for ($i=0; $i < count($res_data); $i++) {
                $get_price  = $this->m_global->get_data_all('service_prices', null, ['servprice_status' => '1', 'servprice_service_id' => $res_data[$i]->id], '*', null, ['servprice_min_pax', 'ASC'])[0];
                $get_image  = $this->m_global->get_data_all('service_images', null, ['servimage_status' => '1', 'servimage_service_id' => $res_data[$i]->id], '*', null, ['servimage_createddate', 'DESC'])[0];
                $res_data[$i]->price        = $get_price->servprice_price;
                $res_data[$i]->thumbnail    = $get_image->servimage_url;
                $res_data[$i]->range_pax    = $get_price->servprice_min_pax.'-'.$get_price->servprice_max_pax;
            }

            // echo $this->db->last_query();

            echo response_builder(true, 200, $res_data);
        }else{
            echo response_builder(false, 900);
        }
    }

    function recomendation(){
        if($this->api_version == '1'){
            $query          = $this->input->post('query');
            $seller         = $this->input->post('seller');
            $storefront     = $this->input->post('storefront');
            $cat_id         = $this->input->post('category_id');

            $select         = 'service_id id, service_name name, seller_name seller, seller_latitude latitude, seller_longitude longitude';
            $join           = [
                                    ['table' => 'sellers', 'on' => 'service_seller_id=seller_id']
                                ];

            $where['service_status']  = '1';

            $res_data       = $this->m_global->get_data_all('services', $join, $where, $select, null, ['service_createddate', 'DESC'], 0, 2);
            for ($i=0; $i < count($res_data); $i++) {
                $get_price  = $this->m_global->get_data_all('service_prices', null, ['servprice_status' => '1', 'servprice_service_id' => $res_data[$i]->id], '*', null, ['servprice_min_pax', 'ASC'])[0];
                $get_image  = $this->m_global->get_data_all('service_images', null, ['servimage_status' => '1', 'servimage_service_id' => $res_data[$i]->id], '*', null, ['servimage_createddate', 'DESC'])[0];
                $res_data[$i]->price        = $get_price->servprice_price;
                $res_data[$i]->thumbnail    = $get_image->servimage_url;
                $res_data[$i]->range_pax    = $get_price->servprice_min_pax.'-'.$get_price->servprice_max_pax;
            }

            echo response_builder(true, 200, $res_data);
        }else if($this->api_version == '2'){
            $latitude       = $this->input->post('latitude');
            $longitude      = $this->input->post('longitude');

            $res_data       = $this->m_travel->get_recomendation($latitude, $longitude);
            for ($i=0; $i < count($res_data); $i++) {
                $get_price  = $this->m_global->get_data_all('service_prices', null, ['servprice_status' => '1', 'servprice_service_id' => $res_data[$i]['id']], '*', null, ['servprice_min_pax', 'ASC'])[0];
                $get_image  = $this->m_global->get_data_all('service_images', null, ['servimage_status' => '1', 'servimage_service_id' => $res_data[$i]['id']], '*', null, ['servimage_createddate', 'DESC'])[0];
                $res_data[$i]['price']        = $get_price->servprice_price;
                $res_data[$i]['thumbnail']    = $get_image->servimage_url;
                $res_data[$i]['range_pax']    = $get_price->servprice_min_pax.'-'.$get_price->servprice_max_pax;
                $res_data[$i]['distance']     = (String) round($res_data[$i]['distance']);
            }

            echo response_builder(true, 200, $res_data);
        }else{
            echo response_builder(false, 900);
        }
    }

    function service_detail(){
        if($this->api_version == '1'){
            $id             = $this->input->post('id');

            $select         = 'service_id id,
                                seller_id id_seller, 
                                service_name name, 
                                seller_name seller, 
                                seller_latitude latitude, 
                                seller_longitude longitude, 
                                seller_avatar avatar,
                                seller_max_pickup_distance max_radius,
                                service_desc descriptions,
                                storefront_id,
                                storefront_name storefront,
                                travelcat_id category_id,
                                travelcat_name category,
                                travelcat_icon_url category_icon
                                ';
            $join           = [
                                    ['table' => 'sellers', 'on' => 'service_seller_id=seller_id'],
                                    ['table' => 'storefront', 'on' => 'service_storefront_id=storefront_id'],
                                    ['table' => 'travel_categories', 'on' => 'travelcat_id=service_category_id']
                                ];

            $res_data           = $this->m_global->get_data_all('services', $join, ['service_id' => $id], $select)[0];
            $res_data->prices   = $this->m_global->get_data_all('service_prices', null, ['servprice_status' => '1', 'servprice_service_id' => $res_data->id], 'servprice_min_pax min_pax, servprice_max_pax max_pax, servprice_price price', null, ['servprice_min_pax', 'ASC']);
            $get_image          = $this->m_global->get_data_all('service_images', null, ['servimage_status' => '1', 'servimage_service_id' => $res_data->id], '*', null, ['servimage_createddate', 'DESC']);
            $res_data->time     = $this->m_global->get_data_all('service_time', null, ['servtime_status' => '1', 'servtime_service_id' => $res_data->id], 'servtime_id id, servtime_time time');
            $thumbnails         = [];

            for ($i=0; $i < count($get_image); $i++) { 
                $thumbnails[] = $get_image[$i]->servimage_url;
            }
            $res_data->thumbnails = $thumbnails;

            echo response_builder(true, 200, $res_data);
        }else{
            echo response_builder(false, 900);
        }
    }

    function seller_detail(){
        if($this->api_version == '1'){
            $id             = $this->input->post('id');

            $select         = 'seller_id id,
                                seller_name seller, 
                                seller_latitude latitude, 
                                seller_longitude longitude, 
                                seller_avatar avatar
                                ';
            $res_data       = $this->m_global->get_data_all('sellers', null, ['seller_id' => $id], $select)[0];

            $storefront[]   = ['id' => '0', 'name' => 'Semua Etalase'];

            $get_storefront = $this->m_global->get_data_all('storefront', null, ['storefront_seller_id' => $id, 'storefront_status' => '1'], 'storefront_id, storefront_name');
            for ($i=0; $i < count($get_storefront); $i++) { 
                $storefront[] = ['id' => $get_storefront[$i]->storefront_id, 'name' => $get_storefront[$i]->storefront_name];
            }
            $res_data->storefront = $storefront;

            echo response_builder(true, 200, $res_data);
        }else{
            echo response_builder(false, 900);
        }
    }

    // function add_storefront(){
    //     if($this->api_version == '1'){
    //         $name           = $this->input->post('name');

    //         $data['storefront_seller_id']   = $this->headers['User-Id'];
    //         $data['storefront_name']        = $name;
    //         $data['storefront_createddate'] = date('Y-m-d H:i:s');

    //         $result                     = $this->m_global->insert('storefront', $data);
    //         if($result['status']){
    //             $id         = $result['id'];
    //             echo response_builder(true, 201, ['id' => $id]);
    //         }else{
    //             echo response_builder(false, 406, null, 'failed create data');
    //         }
    //     }else{
    //         echo response_builder(false, 900);
    //     }
    // }

    // function update_storefront(){
    //     if($this->api_version == '1'){
    //         $storefront_id  = $this->input->post('storefront_id');
    //         $name           = $this->input->post('name');
    //         $status         = $this->input->post('status');

    //         $data['storefront_name']        = $name;
    //         $data['storefront_status']      = $status;

    //         $result                     = $this->m_global->update('storefront', $data, ['storefront_id' => $storefront_id]);
    //         if($result){
    //             echo response_builder(true, 201);
    //         }else{
    //             echo response_builder(false, 406, null, 'failed create data');
    //         }
    //     }else{
    //         echo response_builder(false, 900);
    //     }
    // }

    function add_service(){
        if($this->api_version == '1'){
            $cat_id         = $this->input->post('cat_id');
            $storefront_id  = $this->input->post('storefront_id');
            $name           = $this->input->post('name');
            $desc           = $this->input->post('desc');
            $prices         = json_decode($this->input->post('prices'));
            $thumbnails     = json_decode($this->input->post('thumbnails'));
            $time           = json_decode($this->input->post('time'));

            $date           = date('Y-m-d H:i:s');

            $data['service_category_id']   = $cat_id;
            $data['service_storefront_id'] = $storefront_id;
            $data['service_seller_id']     = $this->headers['User-Id'];
            $data['service_name']          = $name;
            $data['service_desc']          = $desc;
            $data['service_createddate']   = $date;

            $result                     = $this->m_global->insert('services', $data);
            if($result['status']){
                $id         = $result['id'];

                foreach ($thumbnails as $value) {
                    $data_images['servimage_service_id']    = $id;
                    $data_images['servimage_url']           = $value;
                    $data_images['servimage_createddate']   = $date;
                    $this->m_global->insert('service_images', $data_images);
                }

                foreach ($time as $value) {
                    $data_time['servtime_service_id']     = $id;
                    $data_time['servtime_time']           = $value;
                    $data_time['servtime_createddate']    = $date;
                    $this->m_global->insert('service_time', $data_time);
                }

                foreach ($prices as $value) {
                    $data_price['servprice_service_id']     = $id;
                    $data_price['servprice_min_pax']        = $value->min;
                    $data_price['servprice_max_pax']        = $value->max;
                    $data_price['servprice_price']          = $value->price;
                    $data_price['servprice_createddate']    = $date;
                    $this->m_global->insert('service_prices', $data_price);
                }

                echo response_builder(true, 201, ['id' => $id]);
            }else{
                echo response_builder(false, 406, null, 'failed create data');
            }
        }else{
            echo response_builder(false, 900);
        }
    }

    function edit_service(){
        if($this->api_version == '1'){
            $id             = $this->input->post('service_id');
            $cat_id         = $this->input->post('cat_id');
            $storefront_id  = $this->input->post('storefront_id');
            $name           = $this->input->post('name');
            $desc           = $this->input->post('desc');
            $prices         = json_decode($this->input->post('prices'));
            $thumbnails     = json_decode($this->input->post('thumbnails'));
            $time           = json_decode($this->input->post('time'));

            $data['service_category_id']   = $cat_id;
            $data['service_storefront_id'] = $storefront_id;
            $data['service_name']          = $name;
            $data['service_desc']          = $desc;

            $date           = date('Y-m-d H:i:s');

            $result         = $this->m_global->update('services', $data, ['service_id' => $id]);
            if($result){

                $this->m_global->delete('service_images', ['servimage_service_id' => $id]);
                $this->m_global->delete('service_time', ['servtime_service_id' => $id]);
                $this->m_global->delete('service_prices', ['servprice_service_id' => $id]);

                foreach ($thumbnails as $value) {
                    $data_images['servimage_service_id']    = $id;
                    $data_images['servimage_url']           = $value;
                    $data_images['servimage_createddate']   = $date;
                    $this->m_global->insert('service_images', $data_images);
                }

                foreach ($time as $value) {
                    $data_time['servtime_service_id']     = $id;
                    $data_time['servtime_time']           = $value;
                    $data_time['servtime_createddate']    = $date;
                    $this->m_global->insert('service_time', $data_time);
                }

                foreach ($prices as $value) {
                    $data_price['servprice_service_id']     = $id;
                    $data_price['servprice_min_pax']        = $value->min;
                    $data_price['servprice_max_pax']        = $value->max;
                    $data_price['servprice_price']          = $value->price;
                    $data_price['servprice_createddate']    = $date;
                    $this->m_global->insert('service_prices', $data_price);
                }

                echo response_builder(true, 201, ['id' => $id]);
            }else{
                echo response_builder(false, 406, null, 'failed create data');
            }
        }else{
            echo response_builder(false, 900);
        }
    }

    function delete_service(){
        if($this->api_version == '1'){
            $id             = $this->input->post('service_id');
            $data['service_status']        = '99';

            $result         = $this->m_global->update('services', $data, ['service_id' => $id]);
            if($result){
                echo response_builder(true, 201, ['id' => $id]);
            }else{
                echo response_builder(false, 406, null, 'failed delete data');
            }
        }else{
            echo response_builder(false, 900);
        }
    }

    function submit_travel_order(){
        if($this->api_version == '1'){
            $seller_id                          = $this->input->post('seller_id');

            $data['booking_service_id']         = $this->input->post('serv_id');
            $data['booking_buyer_id']           = $this->headers['User-Id'];
            $data['booking_seller_id']          = $seller_id;
            $data['booking_minmax']             = $this->input->post('minmax');
            $data['booking_price']              = $this->input->post('price');
            $data['booking_date']               = $this->input->post('date');
            $data['booking_act_pax']            = $this->input->post('pax');
            $data['booking_special_request']    = $this->input->post('special_req');
            $data['booking_contact_name']       = $this->input->post('cont_name');
            $data['booking_contact_phone']      = $this->input->post('cont_phone');
            $data['booking_contact_email']      = $this->input->post('cont_email');
            $data['booking_pickup_latitude']    = $this->input->post('latitude');
            $data['booking_pickup_longitude']   = $this->input->post('longitude');
            $data['booking_pickup_address']     = $this->input->post('address');
            $data['booking_createddate']        = date('Y-m-d H:i:s');

            $result                             = $this->m_global->insert('bookings', $data);
            if($result['status']){
                $id     = $result['id'];
                $code   = strtoupper(strEncrypt('booking'.$id));
                $this->m_global->update('bookings', ['booking_code' => $code], ['booking_id' => $id]);
                echo response_builder(true, 201, ['code' => $code, 'id' => (String) $id]);
            }else{
                echo response_builder(false, 406, null, 'failed create data');
            }
        }else{
            echo response_builder(false, 900);
        }
    }

    function submit_payment_order(){
        if($this->api_version == '1'){
            $booking_code                       = $this->input->post('code');
            $data['booking_status']             = '1';

            $result                             = $this->m_global->update('bookings', $data, ['booking_code' => $booking_code]);
            if($result){
                $get_data = $this->m_global->get_data_all('bookings', null, ['booking_code' => $booking_code], 'booking_id, booking_seller_id')[0];

                $payload['booking_id'] = $get_data->booking_id; 
                send_notification($get_data->booking_seller_id.'@2', 'Anda Mendapatkan Pesanan Baru', 'Silahkan cek pesanan di halaman Pesanan', 'booking_update', json_encode($payload));
                echo response_builder(true, 201, ['code' => $booking_code]);
            }else{
                echo response_builder(false, 406, null, 'failed update data');
            }
        }else{
            echo response_builder(false, 900);
        }
    }

}

/* End of file config.php */
/* banner: ./application/modules/config/controllers/config.php */