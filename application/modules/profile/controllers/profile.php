<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends CI_Controller {

	function __construct() {
        parent::__construct();
        $this->headers          = $this->input->request_headers();
        $this->api_version      = $this->headers['Api-Version'];
        date_default_timezone_set('UTC');
        header('Content-Type: application/json');
        acc_token();
    }
    

    public function index(){   
        echo "Dolan Profile Rest API";
    }

    function check_quiz(){
        if($this->api_version == '1'){
            $res_data       = $this->m_global->count_data_all('answers', null, ['answer_seller_id' => $this->headers['User-Id']]);
            echo response_builder(true, 200, $res_data);
        }else{
            echo response_builder(false, 900);
        }
    }

    function questions(){
        if($this->api_version == '1'){
            $select         = 'question_id id, question_text text';
            $res_data       = $this->m_global->get_data_all('questions', null, ['question_status' => '1'], $select);
            echo response_builder(true, 200, $res_data);
        }else{
            echo response_builder(false, 900);
        }
    }

    function add_answer(){
        if($this->api_version == '1'){
            $answers        = json_decode($this->input->post('answers'));
            $date           = date('Y-m-d H:i:s');

            foreach ($answers as $value) {
                $data['answer_seller_id']       = $this->headers['User-Id'];
                $data['answer_question_id']     = $value->id;
                $data['answer_text']            = $value->text;
                $data['answer_createddate']     = $date;
                $this->m_global->insert('answers', $data);
            }

            echo response_builder(true, 201);
        }else{
            echo response_builder(false, 900);
        }
    }

    function configure_category(){
        if($this->api_version == '1'){
            $ids        = json_decode($this->input->post('ids'));
            $date       = date('Y-m-d H:i:s');

            foreach ($ids as $value) {
                $data['sellcat_category_id']    = $value;
                $data['sellcat_seller_id']      = $this->headers['User-Id'];
                $data['sellcat_createddate']    = $date;
                $this->m_global->insert('sell_category', $data);
            }

            echo response_builder(true, 201);
        }else{
            echo response_builder(false, 900);
        }
    }

    function get_reply_template(){
        if($this->api_version == '1'){
            $select         = 'reptemp_text text, reptemp_id id';
            $res_data       = $this->m_global->get_data_all('reply_template', null, ['reptemp_seller_id' => $this->headers['User-Id'], 'reptemp_status' => '1'], $select);
            echo response_builder(true, 200, $res_data);
        }else{
            echo response_builder(false, 900);
        }
    }

    function add_reply_template(){
        if($this->api_version == '1'){
            $text           = $this->input->post('text');

            $data['reptemp_seller_id']     = $this->headers['User-Id'];
            $data['reptemp_text']          = $text;
            $data['reptemp_createddate']   = date('Y-m-d H:i:s');

            $result                     = $this->m_global->insert('reply_template', $data);
            if($result['status']){
                echo response_builder(true, 201, ['id' => $result['id']]);
            }else{
                echo response_builder(false, 406, null, 'failed create data');
            }
        }else{
            echo response_builder(false, 900);
        }
    }

    function update_reply_template(){
        if($this->api_version == '1'){
            $id                     = $this->input->post('id');
            $text                   = $this->input->post('text');
            $data['reptemp_text']   = $text;
            $result                 = $this->m_global->update('reply_template', $data, ['reptemp_id' => $id]);
            if($result){
                echo response_builder(true, 209, ['id' => $id]);
            }else{
                echo response_builder(false, 406, null, 'failed');
            }
        }else{
            echo response_builder(false, 900);
        }
    }

    function delete_reply_template(){
        if($this->api_version == '1'){
            $id                         = $this->input->post('id');
            $data['reptemp_status']     = '99';
            $result                     = $this->m_global->update('reply_template', $data, ['reptemp_id' => $id]);
            if($result){
                echo response_builder(true, 209, ['id' => $id]);
            }else{
                echo response_builder(false, 406, null, 'failed');
            }
        }else{
            echo response_builder(false, 900);
        }
    }

    function storefront_list(){
        if($this->api_version == '1'){
            $res_data       = $this->m_global->get_data_all('storefront', null, ['storefront_seller_id' => $this->headers['User-Id'], 'storefront_status' => '1'], 'storefront_id id, storefront_name name');
            for ($i=0; $i < count($res_data); $i++) { 
                $res_data[$i]->total_service = $this->m_global->count_data_all('services', null, ['service_storefront_id' => $res_data[$i]->id, 'service_status' => '1']);
            }
            echo response_builder(true, 200, $res_data);
        }else{
            echo response_builder(false, 900);
        }
    }

    function add_storefront(){
        if($this->api_version == '1'){
            $name           = $this->input->post('name');

            $data['storefront_seller_id']     = $this->headers['User-Id'];
            $data['storefront_name']          = $name;
            $data['storefront_createddate']   = date('Y-m-d H:i:s');

            $result                     = $this->m_global->insert('storefront', $data);
            if($result['status']){
                echo response_builder(true, 201, ['id' => $result['id']]);
            }else{
                echo response_builder(false, 406, null, 'failed create data');
            }
        }else{
            echo response_builder(false, 900);
        }
    }

    function update_storefront(){
        if($this->api_version == '1'){
            $id                         = $this->input->post('id');
            $name                       = $this->input->post('name');
            $data['storefront_name']    = $name;
            $result                     = $this->m_global->update('storefront', $data, ['storefront_id' => $id]);
            if($result){
                echo response_builder(true, 209, ['id' => $id]);
            }else{
                echo response_builder(false, 406, null, 'failed');
            }
        }else{
            echo response_builder(false, 900);
        }
    }

    function delete_storefront(){
        if($this->api_version == '1'){
            $id                         = $this->input->post('id');
            $data['storefront_status']  = '99';
            $result                     = $this->m_global->update('storefront', $data, ['storefront_id' => $id]);
            if($result){
                echo response_builder(true, 209, ['id' => $id]);
            }else{
                echo response_builder(false, 406, null, 'failed');
            }
        }else{
            echo response_builder(false, 900);
        }
    }

    function get_pickup_radius(){
        if($this->api_version == '1'){
            $seller_id  = $this->input->post('seller_id');
            $res_data   = $this->m_global->get_data_all('sellers', null, ['seller_id' => $seller_id], 'seller_max_pickup_distance distance')[0];
            echo response_builder(true, 200, $res_data);
        }else{
            echo response_builder(false, 900);
        }
    }

    function config_pickup_radius(){
        if($this->api_version == '1'){
            $seller_id                          = $this->headers['User-Id'];
            $data['seller_max_pickup_distance'] = $this->input->post('radius');
            $result                     = $this->m_global->update('sellers', $data, ['seller_id' => $seller_id]);
            if($result){
                echo response_builder(true, 209);
            }else{
                echo response_builder(false, 406, null, 'failed');
            }
        }else{
            echo response_builder(false, 900);
        }
    }

    function config_auto_reply(){
        if($this->api_version == '1'){
            $seller_id                  = $this->headers['User-Id'];
            $data['autorep_text']       = $this->input->post('text');
            $data['autorep_seller_id']  = $seller_id;

            $check_data     = $this->m_global->get_data_all('auto_reply', null, ['autorep_seller_id' => $seller_id]);
            if(count($check_data) == 0){
                $data['autorep_createddate']   = date('Y-m-d H:i:s');
                $result                        = $this->m_global->insert('auto_reply', $data);
                if($result['status']){
                    echo response_builder(true, 201, ['id' => $result['id']]);
                }else{
                    echo response_builder(false, 406, null, 'failed create data');
                }
            }else{
                $data['autorep_status']     = '1';
                $result                     = $this->m_global->update('auto_reply', $data, ['autorep_id' => $check_data[0]->autorep_id]);
                if($result){
                    echo response_builder(true, 209, ['id' => $check_data[0]->autorep_id]);
                }else{
                    echo response_builder(false, 406, null, 'failed');
                }
            }
        }else{
            echo response_builder(false, 900);
        }
    }

    function get_auto_reply(){
        if($this->api_version == '1'){
            $seller_id  = $this->input->post('seller_id');
            $res_data   = $this->m_global->get_data_all('auto_reply', null, ['autorep_seller_id' => $seller_id], 'autorep_id id, autorep_text text');
            echo response_builder(true, 200, $res_data);
        }else{
            echo response_builder(false, 900);
        }
    }

    function add_chat_room(){
        if($this->api_version == '1'){
            $seller_id      = $this->input->post('seller_id');
            $buyer_id       = $this->input->post('buyer_id');
            $room           = $this->input->post('room');

            $data['chatroom_seller_id']     = $seller_id;
            $data['chatroom_buyer_id']      = $buyer_id;
            $data['chatroom_name']          = $room;
            $data['chatroom_createddate']   = date('Y-m-d H:i:s');

            $result                     = $this->m_global->insert('chat_room', $data);
            if($result['status']){
                echo response_builder(true, 201, ['id' => $result['id']]);
            }else{
                echo response_builder(false, 406, null, 'failed create data');
            }
        }else{
            echo response_builder(false, 900);
        }
    }

    function chat_room(){
        if($this->api_version == '1'){
            $seller_id      = $this->input->post('seller_id');
            $buyer_id       = $this->input->post('buyer_id');
            $select         = 'chatroom_name room';
            $res_data       = $this->m_global->get_data_all('chat_room', null, ['chatroom_seller_id' => $seller_id, 'chatroom_buyer_id' => $buyer_id], $select);
            echo response_builder(true, 200, $res_data);
        }else{
            echo response_builder(false, 900);
        }
    }

}

/* End of file config.php */
/* banner: ./application/modules/config/controllers/config.php */