<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_booking extends CI_Model {

    public function __construct(){
        parent::__construct();
        $this->headers          = $this->input->request_headers();
    }

    public function get_income($id){
		$sql = "SELECT COALESCE (SUM(booking_price), 0) total_income FROM bookings WHERE `booking_seller_id` = '$id' AND `booking_status` = '4' AND `booking_seller_isdone` = '1'";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
}