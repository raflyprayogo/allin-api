<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bookings extends CI_Controller {

	function __construct() {
        parent::__construct();
        $this->headers          = $this->input->request_headers();
        $this->api_version      = $this->headers['Api-Version'];
        $this->load->model('m_booking');
        date_default_timezone_set('UTC');
        header('Content-Type: application/json');
        acc_token();
    }
    

    public function index(){   
        if($this->api_version == '1'){
            $user_id        = $this->headers['User-Id'];
            $status         = $this->input->post('status');
            $type           = $this->input->post('type');
            $select         = 'booking_id id, booking_code code, service_id package_id, service_name title, travelcat_name category, booking_date date, booking_act_pax pax, booking_status status, booking_price price, travelcat_icon_url thumbnail';
            $join           = [
                                ['table' => 'services', 'on' => 'booking_service_id=service_id'],
                                ['table' => 'travel_categories', 'on' => 'service_category_id=travelcat_id']
                            ];
            $res_data       = $this->m_global->get_data_all('bookings', $join, ['booking_'.$type.'_id' => $user_id], $select, "booking_status IN (".$status.")");
            echo response_builder(true, 200, $res_data);
        }else{
            echo response_builder(false, 900);
        }
    }

    function detail(){
        if($this->api_version == '1'){
            $booking_id     = $this->input->post('booking_id');
            $select         = 'bk.*,
                                sv.service_name,
                                sv.service_id,
                                tc.travelcat_name';
            $join           = [
                                ['table' => 'services sv', 'on' => 'bk.booking_service_id=sv.service_id'],
                                ['table' => 'sellers sl', 'on' => 'bk.booking_seller_id=sl.seller_id'],
                                ['table' => 'travel_categories tc', 'on' => 'sv.service_category_id=tc.travelcat_id']
                            ];
            $res_data       = $this->m_global->get_data_all('bookings bk', $join, ['bk.booking_id' => $booking_id], $select)[0];
            $get_image      = $this->m_global->get_data_all('service_images', null, ['servimage_status' => '1', 'servimage_service_id' => $res_data->service_id], '*', null, ['servimage_createddate', 'DESC'])[0]; 
            $res_data->thumbnail    = $get_image->servimage_url;
            echo response_builder(true, 200, $res_data);
        }else{
            echo response_builder(false, 900);
        }
    }

    function cancel(){
        if($this->api_version == '1'){
            $reason             = $this->input->post('reason');
            $type               = $this->input->post('type');
            $booking_id         = $this->input->post('booking_id');

            $data['booking_status']         = '5';
            $data['booking_cancel_by']      = $type;
            $data['booking_cancel_reason']  = $reason;

            $result   = $this->m_global->update('bookings', $data, ['booking_id' => $booking_id]);
            if($result){

                if($type == '1'){
                    $get_data = $this->m_global->get_data_all('bookings', null, ['booking_id' => $booking_id], 'booking_buyer_id')[0];
                    $payload['booking_id'] = $booking_id; 
                    send_notification($get_data->booking_buyer_id.'@1', 'Pesananmu ditolak Seller', 'Silahkan cek detail pesananmu', 'booking_update', json_encode($payload));
                }

                echo response_builder(true, 201);
            }else{
                echo response_builder(false, 406, null, 'failed update data');
            }
        }else{
            echo response_builder(false, 900);
        }
    }

    function confirm_order_seller(){
        if($this->api_version == '1'){
            $booking_id = $this->input->post('booking_id');
            $data['booking_status']   = '2';
            $result   = $this->m_global->update('bookings', $data, ['booking_id' => $booking_id]);
            if($result){

                $get_data = $this->m_global->get_data_all('bookings', null, ['booking_id' => $booking_id], 'booking_buyer_id')[0];
                $payload['booking_id'] = $booking_id; 
                send_notification($get_data->booking_buyer_id.'@1', 'Pesananmu diterima', 'Silahkan cek detail pesanan & tunggu untuk penjemputan ya', 'booking_update', json_encode($payload));

                echo response_builder(true, 201);
            }else{
                echo response_builder(false, 406, null, 'failed update data');
            }
        }else{
            echo response_builder(false, 900);
        }
    }

    function confirm_start_service_seller(){
        if($this->api_version == '1'){
            $booking_id = $this->input->post('booking_id');
            $data['booking_status']   = '3';
            $result   = $this->m_global->update('bookings', $data, ['booking_id' => $booking_id]);
            if($result){

                $get_data = $this->m_global->get_data_all('bookings', null, ['booking_id' => $booking_id], 'booking_buyer_id')[0];
                $payload['booking_id'] = $booking_id; 
                send_notification($get_data->booking_buyer_id.'@1', 'Layanan dimulai', 'Selamat menikmati layanan dari Seller :)', 'booking_update', json_encode($payload));

                echo response_builder(true, 201);
            }else{
                echo response_builder(false, 406, null, 'failed update data');
            }
        }else{
            echo response_builder(false, 900);
        }
    }

    function confirm_done_service_seller(){
        if($this->api_version == '1'){
            $booking_id = $this->input->post('booking_id');
            $data['booking_seller_isdone']   = '1';
            $result   = $this->m_global->update('bookings', $data, ['booking_id' => $booking_id]);
            if($result){

                $get_data = $this->m_global->get_data_all('bookings', null, ['booking_id' => $booking_id], 'booking_buyer_id')[0];
                $payload['booking_id'] = $booking_id; 
                send_notification($get_data->booking_buyer_id.'@1', 'Layanan selesai ..., terima kasih :)', 'Silahkan konfirmasi layanan selesai di halaman pesananmu ya ...', 'booking_update', json_encode($payload));

                echo response_builder(true, 201);
            }else{
                echo response_builder(false, 406, null, 'failed update data');
            }
        }else{
            echo response_builder(false, 900);
        }
    }

    function confirm_done_service_buyer(){
        if($this->api_version == '1'){
            $booking_id = $this->input->post('booking_id');
            $data['booking_status']   = '4';
            $result   = $this->m_global->update('bookings', $data, ['booking_id' => $booking_id]);
            if($result){

                $get_data = $this->m_global->get_data_all('bookings', null, ['booking_id' => $booking_id], 'booking_seller_id, booking_buyer_id')[0];
                $payload['booking_id'] = $booking_id; 
                send_notification($get_data->booking_seller_id.'@2', 'Layanan selesai ..., terima kasih :)', 'Silahkan cek dashboard untuk melihat hasil layananmu', 'booking_update', json_encode($payload));
                send_notification($get_data->booking_buyer_id.'@1', 'Terima kasih telah menggunakan Allin', 'Bagikan pengalamanmu menggunakan Allin ke teman-temanmu :)', 'booking_update', json_encode($payload));

                echo response_builder(true, 201);
            }else{
                echo response_builder(false, 406, null, 'failed update data');
            }
        }else{
            echo response_builder(false, 900);
        }
    }

    function generate_link_midtrans(){
        if($this->api_version == '1'){
            $booking_id           = $this->input->post('booking_id');
            $booking_code         = $this->input->post('booking_code');
            $amount               = $this->input->post('amount');

            $check_payment        = $this->m_global->get_data_all('payment_log', null, ['paylog_booking_id' => $booking_id]);
            if(count($check_payment) == 0){    

                $fields['credit_card'] = ['secure' => true];
                $fields['transaction_details'] = ['gross_amount' => (Int) $amount, 'order_id' => $booking_code];
                $headers = ['Authorization: Basic U0ItTWlkLXNlcnZlci1xRkF6OFpEaWhYWDhxYzBnaHVBdU1IbnU6','Content-Type: application/json'];
                $ch = curl_init();
                curl_setopt( $ch, CURLOPT_URL, 'https://app.sandbox.midtrans.com/snap/v1/transactions' );
                curl_setopt( $ch, CURLOPT_POST, true );
                curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
                curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields ) );
                curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, false);
                curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false);
                $result = curl_exec($ch);
                curl_close($ch);

                $res_data = json_decode($result, true);

                if(isset($res_data['token'])){
                    $data['paylog_booking_id']  = $booking_id;
                    $data['paylog_token']       = $res_data['token'];
                    $this->m_global->insert('payment_log', $data);

                    echo response_builder(true, 200, $res_data);
                }else{
                    echo response_builder(false, 406, null, $res_data['error_messages']);
                }
            }else{
                $token = $check_payment[0]->paylog_token;

                $res_data['token'] = $token;
                $res_data['redirect_url'] = 'https://app.sandbox.midtrans.com/snap/v2/vtweb/'.$token;

                echo response_builder(true, 200, $res_data);
            }
        }else{
            echo response_builder(false, 900);
        }
    }

    public function seller_status(){   
        if($this->api_version == '1'){
            $user_id        = $this->headers['User-Id'];
            $res_data['account_status']    = $this->m_global->get_data_all('sellers', null, ['seller_id' => $user_id], 'seller_status')[0]->seller_status;
            echo response_builder(true, 200, $res_data);
        }else{
            echo response_builder(false, 900);
        }
    }

    public function seller_dashboard(){   
        if($this->api_version == '1'){
            $user_id        = $this->headers['User-Id'];
            $res_data['account_status']    = $this->m_global->get_data_all('sellers', null, ['seller_id' => $user_id], 'seller_status')[0]->seller_status;
            $res_data['total_new_order']   = $this->m_global->count_data_all('bookings', null, ['booking_seller_id' => $user_id, 'booking_status' => '1']);
            $res_data['total_ready_pick']  = $this->m_global->count_data_all('bookings', null, ['booking_seller_id' => $user_id, 'booking_status' => '2']);
            $res_data['total_rejected']    = $this->m_global->count_data_all('bookings', null, ['booking_seller_id' => $user_id, 'booking_status' => '5', 'booking_cancel_by' => '1']);
            $res_data['total_completed']   = $this->m_global->count_data_all('bookings', null, ['booking_seller_id' => $user_id, 'booking_status' => '4', 'booking_seller_isdone' => '1']);
            $res_data['total_income']      = $this->m_booking->get_income($user_id)[0]['total_income'];

            echo response_builder(true, 200, $res_data);
        }else{
            echo response_builder(false, 900);
        }
    }

}

/* End of file config.php */
/* banner: ./application/modules/config/controllers/config.php */