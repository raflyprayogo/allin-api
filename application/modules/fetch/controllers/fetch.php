<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Fetch extends MX_Controller {

    private $rule_valid     = 'xss_clean|encode_php_tags';

    function __construct() 
    {
        parent::__construct();

    }

    /* Start Fetch Core Controller */

    public function index()
    {   
        return false;
    }

    public function religion( $id = NULL )
    {
        $tmp    = [
                    ['id' => 'Islam', 'name' => 'Islam'],
                    ['id' => 'Protestan', 'name' => 'Protestan'],
                    ['id' => 'Katolik', 'name' => 'Katolik'],
                    ['id' => 'Hindu', 'name' => 'Hindu'],
                    ['id' => 'Budha', 'name' => 'Budha'],
                    ['id' => 'Konghucu', 'name' => 'Konghucu']
                ];

        if ( is_null( $id ) )
        {

            echo json_encode( ['item' => $tmp] );

        } else {

            $hasil = [['id' => $id, 'name' => $id ]];

            echo json_encode( $hasil );

        }
    }

    public function partner( $id = null )
    {
        if ( is_null( $id ) )
        {
            $q       = $_GET['q'];
            $records = $this->m_global->get_data_all( 'partner', NULL, ['partner_name LIKE' => "%$q%"], '*', null, null, 0, 5 );

            $data = [];
        
            for ( $i = 0; $i < count( $records ); $i++ ) 
            {
                $data[$i] = ['id' => $records[$i]->partner_id, 'name' => $records[$i]->partner_name];
            }

            echo json_encode( ['item' => $data] );
        }
        else
        {
            $records    = $this->m_global->get_data_all( 'partner', null, ['partner_id' => $id]);
            $tmp        = [
                            ['id' => $id, 'name' => $records[0]->partner_name]
                        ];

            echo json_encode( $tmp );
        }
    }

    public function order_code( $id = null )
    {
        if ( is_null( $id ) )
        {
            $q       = $_GET['q'];
            $records = $this->m_global->get_data_all( 'order', NULL, ['order_code LIKE' => "%$q%"], '*', null, null, 0, 5 );

            $data = [];
        
            for ( $i = 0; $i < count( $records ); $i++ ) 
            {
                $data[$i] = ['id' => $records[$i]->order_id, 'name' => $records[$i]->order_code];
            }

            echo json_encode( ['item' => $data] );
        }
        else
        {
            $records    = $this->m_global->get_data_all( 'order', null, ['order_id' => $id]);
            $tmp        = [
                            ['id' => $id, 'name' => $records[0]->order_code]
                        ];

            echo json_encode( $tmp );
        }
    }

}

/* End of file config.php */
/* Location: ./application/modules/config/controllers/config.php */