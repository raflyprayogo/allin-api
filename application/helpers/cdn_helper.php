<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
    START Core Helper        
*/

function csrf_init(){
    $CI =& get_instance();  

    $csrf   = strEncrypt('csrf');
    $value  = strEncrypt(date('YmdHis'));

    $CI->session->unset_userdata($csrf);    
    $CI->session->set_userdata([$csrf => $value]);
}

function csrf_get_token(){
    $CI =& get_instance();
    $csrf   = strEncrypt('csrf');
    $data   = @$CI->session->userdata($csrf);

    $data   = ($data != '') ? $data : '-';

    return $data;
}

function strEncrypt($str, $forDB = FALSE){
    $CI =& get_instance();  
    $key    = $CI->config->item('encryption_key');

    $str    = ($forDB) ? 'md5(concat(\'' . $key . '\',' . $str . '))' : md5($key . $str);   
    return substr($str,0,14);
}

function generate_salt(){
    $CI =& get_instance();  
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < 16; $i++) {
    $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function md5_mod($str, $salt){

	$str = md5(md5($str).$salt);
	return $str;
}

function generate_sesscode($emp_id, $session, $day){
    $day = explode('-', $day);
    $day = $day[0].(strlen($day[1]) == 1 ? '0'.$day[1]: $day[1]).(strlen($day[2]) == 1 ? '0'.$day[2]: $day[2]);
    return md5_mod($session, $emp_id.$day);
}

function bulan($bulan)
{
    $aBulan = ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];
    
    return $aBulan[$bulan];
}

function bulan_min($bulan)
{
    $aBulan = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
    
    return $aBulan[$bulan];
}

function tgl_format( $tgl, $showtime = 0, $nomin = null )
{
    if( !is_null($tgl) and $tgl != '' and $tgl != '0000-00-00' ){

    $tanggal    = date('d', strtotime($tgl));
    $bulan      = ( $nomin == null ? bulan_min( date('n', strtotime($tgl))-1 ) : bulan( date('n', strtotime($tgl))-1 ) );
    $tahun      = ( $nomin == null ? date('y', strtotime($tgl)) : date('Y', strtotime($tgl)) );

    $hasil      = $tanggal.' '.$bulan.' '.$tahun;

        if ( $showtime == 1 ) {
            $jam    = ' ' . substr($tgl, -8) . '';
            $jam    = explode(':', $jam);
            $hasil  = ( $showtime == 1 ) ? $hasil . @$jam[0].':'.@$jam[1] : $hasil;
        }

    } else {
        $hasil = '';
    }

    return $hasil;
}

function tgl_format_day($tgl){
    $dayIndo = ['Sun' => 'Sunday', 'Mon' => 'Monday', 'Tue' => 'Tuesday', 'Wed' => 'Wednesday', 'Thu' => 'Thursday', 'Fri' => 'Friday', 'Sat' => 'Saturday'];
    $hasil = date('D', strtotime($tgl));

    return $dayIndo[$hasil];
}

function religion_to_index($index)
{
    $religion = [
    'Islam'     => '1',
    'Protestan' => '2',
    'Katolik'   => '3',
    'Hindu'     => '4',
    'Budha'     => '5',
    'Konghucu'  => '6'
    ];

    return $religion[$index];
}

function multi_encript($id)
{
    $data = [];
    foreach ($id as $key => $value) {
        $data[] = strEncrypt($value);
    }

    return $data;
}

function display($var, $exit = null)
{
    echo '<pre>';print_r($var);echo '</pre>';
    if ( $exit )
    {
        exit();
    }
}

/*
    END Core Helper        
*/

function in_array_r($needle, $haystack, $strict = false) {
    foreach ($haystack as $item) {
        if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && in_array_r($needle, $item, $strict))) {
            return true;
        }
    }

    return false;
}

function Terbilang($x)
{
    $abil = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
    if ($x < 12)
        return " " . $abil[$x];
    elseif ($x < 20)
        return Terbilang($x - 10) . "belas";
    elseif ($x < 100)
        return Terbilang($x / 10) . " puluh" . Terbilang($x % 10);
    elseif ($x < 200)
        return " seratus" . Terbilang($x - 100);
    elseif ($x < 1000)
        return Terbilang($x / 100) . " ratus" . Terbilang($x % 100);
    elseif ($x < 2000)
        return " seribu" . Terbilang($x - 1000);
    elseif ($x < 1000000)
        return Terbilang($x / 1000) . " ribu" . Terbilang($x % 1000);
    elseif ($x < 1000000000)
        return Terbilang($x / 1000000) . " juta" . Terbilang($x % 1000000);
}

function rgb2hex($rgb) {
   $hex = '';
   $hex .= str_pad(dechex($rgb[0]), 2, "0", STR_PAD_LEFT);
   $hex .= str_pad(dechex($rgb[1]), 2, "0", STR_PAD_LEFT);
   $hex .= str_pad(dechex($rgb[2]), 2, "0", STR_PAD_LEFT);

   return $hex;
}

function sidebar_menu( $menu, $url )
{
    foreach ( $menu as $key => $value ) 
    {
        echo 
            '<li ' . 
            /*
                Jika nama controller dari menu helper sama dengan controller
            */
            ( $value['controller'] == $url 
                ? 'class="start active open"' 
                : ''
            ).'>

            <a ' .
            /*
                Mempunyai sub menu atau tidak
                untuk link href
            */
            (is_array($value['link']) 
                ? 'href="javascript:;"' 
                : 'class="ajaxify" href="'.base_url($value['link']).'"') . 
            '">

            <i class="icon-'.$value['icon'].'"></i>

            <span class="title">'.$value['name'].'</span>' .

            /*
                Mempunyai sedang aktif
            */
            ($key == 0 
                ? '<span class="selected"></span>' 
                : ''
            ) .

            /*
                Mempunyai sub menu atau tidak
                untuk menampilkan arrow
            */
            (is_array($value['link']) 
                ? '<span class="arrow ' .
                    ( $value['controller'] == $url 
                    ? 'open'
                    : '')
                . '"></span>'
                : ''
            ) . '</a>';
            
            sub_menu( $value, $url, '2' );

        echo '</li>';
    }
}

function sub_menu( $value, $url, $segment ){

    /*
        Mempunyai sub menu atau tidak
        untuk menampilkan sub link
    */

    if ( is_array($value['link']) )
    {
        echo '<ul class="sub-menu">';

        $CI =& get_instance();

        /*
            Menampilkan sub menu
        */

        foreach ( $value['link'] as $kSub => $kValue ) 
        {
            $sub_url = $CI->uri->segment($segment);

            /*
                Jika controller parent sama dengan uri sebelumnya
                dan controller sekarang sama dengan uri sekarang
            */

            echo '<li ' .
                ($kValue['controller'] == $sub_url && $value['controller'] == $url 
                    ? 'class="active"' 
                    : ''
                ) . '>

                <a ' .

                /*
                    Jika mempunyai sub, maka href=javascript (tidak ada link)
                    jika tidak, maka href berisi link
                */

                (is_array($kValue['link']) 
                    ? 'href="javascript:;"' 
                    : 'class="ajaxify" href="'.base_url($kValue['link']).'"'
                ) . 

                '">
                    <i class="icon-'.$kValue['icon'].'"></i>
                    ' . $kValue['name'] .

                /*
                    Jika mempunyai sub dan controller parent sama dengan uri sekarang
                    maka arrow open (sub menu sedang aktif)
                    selain itu, hanya menampilkan arrow (mempunyai sub menu tapi tidak aktif)
                */

                (is_array($kValue['link']) && $kValue['controller'] == $sub_url  
                    ? '<span class="arrow open"></span>' 
                    : 
                        (is_array($kValue['link'] ) 
                            ? '<span class="arrow"></span>'
                            : ''
                        )
                ) . '
                    </a>';
                
                /*
                    cek lagi gan sub menu level selanjutnya
                */
                    
                sub_menu( $kValue, $sub_url, $segment+1 );

             echo '</li>';
        }
        echo '</ul>';
    }
    
}

function cek_interfal( $start, $end )
{
    $date1      = date_create( $start );
    $date2      = date_create( $end );
    $interfal   = date_diff( $date1, $date2 );

    $year       = ( $interfal->format('%y') != '0' ? $interfal->format('%y') .' Tahun ' : '');
    $mount      = ( $interfal->format('%m') != '0' ? $interfal->format('%m') .' Bulan ' : '');
    $day        = ( $interfal->format('%d') != '0' ? $interfal->format('%d') .' Hari ' : '');
    $hours      = ( $interfal->format('%h') != '0' ? $interfal->format('%h') .' Jam ' : '');
    $minute     = ( $interfal->format('%i') != '0' ? $interfal->format('%i') .' Menit ' : '');
    $second     = ( $interfal->format('%s') != '0' ? $interfal->format('%s') .' Detik ' : '');

    $hasil      = $year.$mount.$day.$hours.$minute.$second;

    return $hasil;
}

function cek_interfal_minim( $start, $end )
{
    $date1      = date_create( $start );
    $date2      = date_create( $end );
    $interfal   = date_diff( $date1, $date2 );

    $year       = ( $interfal->format('%y') != '0' ? $interfal->format('%y') .' thn ' : null);
    $mount      = ( $interfal->format('%m') != '0' ? $interfal->format('%m') .' bln ' : null);
    $day        = ( $interfal->format('%d') != '0' ? $interfal->format('%d') .' hr ' : null);
    $hours      = ( $interfal->format('%h') != '0' ? $interfal->format('%h') .' jam ' : null);
    $minute     = ( $interfal->format('%i') != '0' ? $interfal->format('%i') .' mnt ' : null);
    $second     = ( $interfal->format('%s') != '0' ? $interfal->format('%s') .' dtk' : null);

    $hasil      = ( $year ? $year : ( $mount ? $mount : ( $day ? $day : ( $hours ? $hours : ( $minute ? $minute : ( $second ? $second : '') ) ) ) ) );

    return $hasil;
}

function getAuth( $index = null )
{
    $CI =& get_instance();
    $session = $CI->session->all_userdata();
    if(is_null($index)){
        return $session;
    } else {
        return $session[$index];
    }
}

function checkAuth($prefix, $role){
    if($prefix == 'user'){
        if($role == '0'){
            return true;
        }else{
            redirect( base_url('dashboard') );
            exit;
        }
    }else if($prefix == 'banner'){
        if($role == '0' || $role == '2'){
            return true;
        }else{
            redirect( base_url('dashboard') );
            exit;
        }
    }else if($prefix == 'customer'){
        if($role == '0' || $role == '3'){
            return true;
        }else{
            redirect( base_url('dashboard') );
            exit;
        }
    }else if($prefix == 'driver'){
        if($role == '0' || $role == '1'){
            return true;
        }else{
            redirect( base_url('dashboard') );
            exit;
        }
    }else if($prefix == 'location'){
        if($role == '0'){
            return true;
        }else{
            redirect( base_url('dashboard') );
            exit;
        }
    }else if($prefix == 'order'){
        if($role == '0' || $role == '3'){
            return true;
        }else{
            redirect( base_url('dashboard') );
            exit;
        }
    }else if($prefix == 'partner'){
        if($role == '0'){
            return true;
        }else{
            redirect( base_url('dashboard') );
            exit;
        }
    }else if($prefix == 'partner_dashboard'){
        if($role == '4'){
            return true;
        }else{
            redirect( base_url('dashboard') );
            exit;
        }
    }else if($prefix == 'incident'){
        if($role == '0'){
            return true;
        }else{
            redirect( base_url('dashboard') );
            exit;
        }
    }else if($prefix == 'ds_price'){
        if($role == '0'){
            return true;
        }else{
            redirect( base_url('dashboard') );
            exit;
        }
    }else if($prefix == 'promocode'){
        if($role == '0'){
            return true;
        }else{
            redirect( base_url('dashboard') );
            exit;
        }
    }else if($prefix == 'order_driver_service'){
        if($role == '0'){
            return true;
        }else{
            redirect( base_url('dashboard') );
            exit;
        }
    }
}

function getEmp( $index = null )
{
    $CI =& get_instance();
    $session = $CI->session->all_userdata();
    if(is_null($index)){
        return $session['user_data'];
    } else {
        $name = 'employee_'.$index;
        return $session['user_data']->$name;
    }
}

function uang($var,$dec="0"){
    if(empty($var)) return 'Rp. 0';
    return 'Rp. ' . number_format(str_replace(',','.',$var),$dec,',','.');
}

function dis_uang($var){
    $set1 = str_replace('Rp. ', '', $var);
    $set2 = str_replace('.', '', $set1);
    return $set2;
}

function gcm_sender($fields){
    $headers = ['Authorization: key=' . GCM_API,'Content-Type: application/json'];
    $ch = curl_init();
    curl_setopt( $ch, CURLOPT_URL, GCM_URL );
    curl_setopt( $ch, CURLOPT_POST, true );
    curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
    curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields ) );
    curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false);
    $result = curl_exec($ch);
    curl_close($ch);

    return $result;
}

function gcm_sender_ios($fields){
    // $fields['content_available']    = true;
    // $fields['priority']             = 'high';
    // $fields['mutable_content']      = true;

    if(isset($fields['data']['message']) && isset($fields['data']['title'])){
        $fields['notification']         = [ 'text' => $fields['data']['message'], 'title' => $fields['data']['title']. (isset($fields['data']['info']) ? ' ('.$fields['data']['info'].')': ''),'sound' => 'default', 'icon' => 'ic_valetic_logo', 'color' => '#00838f'];
    }else{
        if(isset($fields['data']['status'])){
            $status = $fields['data']['status'];
            if($status == '2'){
                $fields['notification']         = [ 'text' => 'Price depends on service durations', 'title' => 'Driver start driving','sound' => 'default'];
            }else if($status == '3'){
                $fields['notification']         = [ 'text' => 'Click to know the durations and price', 'title' => 'Finished driving','sound' => 'default'];
            }else if($status == '4'){
                $fields['notification']         = [ 'text' => 'Dont forget to give a rating to the driver', 'title' => 'Order complete','sound' => 'default'];
            }else{
                $fields['notification']         = [ 'text' => '...', 'title' => '...'];
            }

        }else{
            $fields['notification']         = [ 'text' => '...', 'title' => '...'];
        }
    }

    $headers = ['Authorization: key=' . GCM_API,'Content-Type: application/json'];
    $ch = curl_init();
    curl_setopt( $ch, CURLOPT_URL, GCM_URL );
    curl_setopt( $ch, CURLOPT_POST, true );
    curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
    curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields ) );
    curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false);
    $result = curl_exec($ch);
    curl_close($ch);

    return $result;
}

function generate_file_token($token){
    $content = $token;
    file_put_contents('assets/files/tokens/'.$token.'.json',$token);
}

function check_token($token, $data){
    if($token != ''){
        if(file_exists('assets/files/tokens/'.$token.'.json')  == ''){
            echo 'error parsing data'; 
            exit;
        }else{
            return $data;
        }
    }else{
        echo 'error parsing data'; 
        exit;
    }
}

function secure_str($str) {
    $result = trim(htmlspecialchars(strip_tags($str)));

    return $result;
}

function diff_time_work($datenow){
    $datetime1 = strtotime($datenow);
    $datetime2 = strtotime(date('Y-m-d H:i:s'));
    $interval  = abs($datetime2 - $datetime1);
    $minutes   = round($interval / 60);
    return $minutes;
}

function diff_time_work_param($date1, $date2){
    $datetime1 = strtotime($date1);
    $datetime2 = strtotime($date2);
    $interval  = abs($datetime2 - $datetime1);
    $minutes   = round($interval / 60);
    return $minutes;
}

function time_digit($time){
    $explode = explode(':', $time);
    return $explode[0].':'.$explode[1];
}

function minute_to_hours($t){
    if($t == '0'){
        return '0 hours';
    }else{
        $h = floor($t/60) ? floor($t/60) .' hours' : '';
        $m = $t%60 ? $t%60 .' minutes' : '';
        return $h && $m ? $h.' '.$m : $h.$m;
    }
}

function send_response($status, $api_version, $data=null, $msg='', $response_code=200){
    $response['success']    = $status;
    $response['version']    = $api_version;
    $response['message']    = $msg;
    $response['code']       = $response_code;
    $response['data']       = $data;
    echo json_encode($response);
}

function get_age($dob){ 
    $birthdate  = new DateTime(date("Y-m-d",  strtotime($dob)));
    $today      = new DateTime(date("Y-m-d",  strtotime(date('Y-m-d'))));           
    $age        = $birthdate->diff($today)->y;
    return $age;
}

function acc_token(){
    $CI =& get_instance();
    $header         = $CI->input->request_headers();
    $token          = $header['Api-Token'];
    $id             = $header['User-Id'];
    $no             = $header['Email'];
    $fcm_token      = $header['Fcm-Token'];
    $model_name     = $header['Model-Name'];
    if($token != ''){
        if(file_exists(URL_TOKENS.$token.'.json')  == ''){
            echo response_builder(false, 401, null, 'token not available');
            exit;
        }else{
            $salt    = $fcm_token.'%'.$model_name;
            $ctoken  = md5_mod($salt, $id.'%'.$no);
            if($ctoken == $token){
                return true;
            }else{
                echo response_builder(false, 401, null, 'token dont match');
                exit;
            }
        }
    }else{
        echo response_builder(false, 401, null, 'token not in header');
        exit;
    }
}

function response_builder($status, $code, $data = null, $add_msg=""){
    $CI =& get_instance();
    $headers = $CI->input->request_headers();
    return json_encode([
        'status'            => $status,
        'response_code'     => $code,
        'response_msg'      => response_code($code), 
        'additional_msg'    => $add_msg, 
        'data'              => $data,
    ]);
}

function response_code($code){
    $http_status_codes = array(
        100 => 'Informational: Continue',
        101 => 'Informational: Switching Protocols',
        102 => 'Informational: Processing',
        200 => 'Successful: OK',
        201 => 'Successful: Created',
        209 => 'Successful: Updated',
        202 => 'Successful: Accepted',
        203 => 'Successful: Non-Authoritative Information',
        204 => 'Successful: No Content',
        205 => 'Successful: Reset Content',
        206 => 'Successful: Partial Content',
        207 => 'Successful: Multi-Status',
        208 => 'Successful: Already Reported',
        226 => 'Successful: IM Used',
        300 => 'Redirection: Multiple Choices',
        301 => 'Redirection: Moved Permanently',
        302 => 'Redirection: Found',
        303 => 'Redirection: See Other',
        304 => 'Redirection: Not Modified',
        305 => 'Redirection: Use Proxy',
        306 => 'Redirection: Switch Proxy',
        307 => 'Redirection: Temporary Redirect',
        308 => 'Redirection: Permanent Redirect',
        400 => 'Client Error: Bad Request',
        401 => 'Client Error: Unauthorized',
        402 => 'Client Error: Payment Required',
        403 => 'Client Error: Forbidden',
        404 => 'Client Error: Not Found',
        405 => 'Client Error: Method Not Allowed',
        406 => 'Client Error: Not Acceptable',
        407 => 'Client Error: Proxy Authentication Required',
        408 => 'Client Error: Request Timeout',
        409 => 'Client Error: Conflict',
        410 => 'Client Error: Gone',
        411 => 'Client Error: Length Required',
        412 => 'Client Error: Precondition Failed',
        413 => 'Client Error: Request Entity Too Large',
        414 => 'Client Error: Request-URI Too Long',
        415 => 'Client Error: Unsupported Media Type',
        416 => 'Client Error: Requested Range Not Satisfiable',
        417 => 'Client Error: Expectation Failed',
        418 => 'Client Error: I\'m a teapot',
        419 => 'Client Error: Authentication Timeout',
        420 => 'Client Error: Method Failure',
        422 => 'Client Error: Unprocessable Entity',
        423 => 'Client Error: Locked',
        424 => 'Client Error: Method Failure',
        425 => 'Client Error: Unordered Collection',
        426 => 'Client Error: Upgrade Required',
        428 => 'Client Error: Precondition Required',
        429 => 'Client Error: Too Many Requests',
        431 => 'Client Error: Request Header Fields Too Large',
        444 => 'Client Error: No Response',
        449 => 'Client Error: Retry With',
        450 => 'Client Error: Blocked by Windows Parental Controls',
        451 => 'Client Error: Redirect',
        494 => 'Client Error: Request Header Too Large',
        495 => 'Client Error: Cert Error',
        496 => 'Client Error: No Cert',
        497 => 'Client Error: HTTP to HTTPS',
        499 => 'Client Error: Client Closed Request',
        500 => 'Server Error: Internal Server Error',
        501 => 'Server Error: Not Implemented',
        502 => 'Server Error: Bad Gateway',
        503 => 'Server Error: Service Unavailable',
        504 => 'Server Error: Gateway Timeout',
        505 => 'Server Error: HTTP Version Not Supported',
        506 => 'Server Error: Variant Also Negotiates',
        507 => 'Server Error: Insufficient Storage',
        508 => 'Server Error: Loop Detected',
        509 => 'Server Error: Bandwidth Limit Exceeded',
        510 => 'Server Error: Not Extended',
        511 => 'Server Error: Network Authentication Required',
        598 => 'Server Error: Network read timeout error',
        599 => 'Server Error: Network connect timeout error',
        900 => 'Server Error: Api version not available',
    );

    return $http_status_codes[$code];
}

function insert_notif_list($target_id, $sender_id, $type, $object_id){
    $CI =& get_instance();
    $data['notif_target_id']    = $target_id;
    $data['notif_sender_id']    = $sender_id;
    $data['notif_type']         = $type;
    $data['notif_object_id']    = $object_id;
    $data['notif_createddate']  = date('Y-m-d H:i:s');
    $CI->m_global->insert('notification', $data);
}

function send_notification($receiver_id, $title, $msg,$type,$payload){
    $CI =& get_instance();
    $devices        = [];
    $receiver_id    =explode("@", $receiver_id);

    if($receiver_id == 'all'){
        $data       = $CI->m_global->get_data_all('devices', null, ['device_status' => '1']);
    }else{
        $data       = $CI->m_global->get_data_all('devices', null, ['device_user_id' => $receiver_id[0], 'device_user_type' => $receiver_id[1], 'device_status' => '1']);
    }
    for ($i=0; $i < count($data); $i++) { 
        $devices[] = $data[$i]->device_fcm_token;
    }
    $fields = [
        'registration_ids'  => $devices,
        'data'              => [    
                                    'title' => $title,
                                    'message' => $msg,
                                    'type' => $type,
                                    'target_user' => $receiver_id[0],
                                ],
    ];

    $payload       = json_decode($payload, true);
    if(count($payload) > 0){
        $fields['data']     = array_merge($fields['data'], $payload);
    }

    $result = gcm_sender($fields);
    $result = json_decode($result);
    // echo json_encode($fields);
}